from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import Member


class MemberInline(admin.StackedInline):
    model = Member
    can_delete = False
    verbose_name_plural = 'members'


class UserAdmin(BaseUserAdmin):
    list_display = ('username', 'credits', 'date_joined', 'last_login',
                    'is_staff', 'is_active')
    list_filter = ('is_active', 'is_staff')
    inlines = (MemberInline,)

    def credits(self, obj):
        return obj.member.credits
    credits.short_description = 'Crédits'
    credits.admin_order_field = 'member__credits'


admin.site.unregister(User)
admin.site.register(User, UserAdmin)

import datetime

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                verbose_name='Utilisateur')
    credits = models.IntegerField(_('Crédits'))

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name = _('Membre')
        verbose_name_plural = _('Membres')


class PracticeStats(models.Model):
    uuid = models.CharField(_('UUID'), max_length=100, primary_key=True)
    username = models.CharField(_('Nom d\'utilisateur'), max_length=100)
    kills = models.IntegerField(_('Meutres'))
    deaths = models.IntegerField(_('Morts'))
    lms = models.IntegerField(_('lms'))
    brackets = models.IntegerField(_('brackets'))
    party_vs_party_wins = models.IntegerField(_('party_vs_party_wins'))
    global_elo = models.IntegerField(
        _('global_elo'))
    elo_nodebuffelo1v1 = models.IntegerField(
        _('elo_nodebuffelo1v1'))
    elo_builduhcelo1v1 = models.IntegerField(
        _('elo_builduhcelo1v1'))
    elo_archerelo1v1 = models.IntegerField(
        _('elo_archerelo1v1'))
    elo_gappleelo1v1 = models.IntegerField(
        _('elo_gappleelo1v1'))
    elo_comboelo1v1 = models.IntegerField(
        _('elo_comboelo1v1'))
    elo_combosumoelo1v1 = models.IntegerField(
        _('elo_combosumoelo1v1'))
    elo_sumoelo1v1 = models.IntegerField(
        _('elo_sumoelo1v1'))
    elo_spleefelo1v1 = models.IntegerField(
        _('elo_spleefelo1v1'))
    elo_nodebuffelo1v1premium = models.IntegerField(
        _('elo_nodebuffelo1v1premium'))
    elo_builduhcelo1v1premium = models.IntegerField(
        _('elo_builduhcelo1v1premium'))
    elo_archerelo1v1premium = models.IntegerField(
        _('elo_archerelo1v1premium'))
    elo_gappleelo1v1premium = models.IntegerField(
        _('elo_gappleelo1v1premium'))
    elo_comboelo1v1premium = models.IntegerField(
        _('elo_comboelo1v1premium'))
    elo_combosumoelo1v1premium = models.IntegerField(
        _('elo_combosumoelo1v1premium'))
    elo_sumoelo1v1premium = models.IntegerField(
        _('elo_sumoelo1v1premium'))
    elo_spleefelo1v1premium = models.IntegerField(
        _('elo_spleefelo1v1premium'))

    def __str__(self):
        return _('Statistiques Practice de « {username} »').format(
            username=self.username)

    class Meta:
        app_label = 'practice'
        db_table = 'stats'
        verbose_name = _('Statistique Practice')
        verbose_name_plural = _('Statistiques Practice')


class PracticeFight(models.Model):
    playback = models.CharField(_('Playback'), max_length=100,
                                primary_key=True)
    winners = models.CharField(_('Gagnants'), max_length=1000)
    losers = models.IntegerField(_('Perdants'), max_length=1000)
    kit_info = models.IntegerField(_('Informations sur le kit'))
    arena = models.IntegerField(_('Arène'))
    started = models.CharField(_('Début'))
    ended = models.IntegerField(_('Fin'))
    winners_old_elos = models.IntegerField(_('ELO des gagnants avant le '
                                             'combat'))
    losers_old_elos = models.IntegerField(_('ELO des perdants avant le '
                                            'combat'))
    winners_new_elos = models.IntegerField(_('ELO des gagnants après le '
                                             'combat'))
    losers_new_elos = models.IntegerField(_('ELO des perdants après le '
                                            'combat'))
    inventories = models.IntegerField(_('Inventaires'))

    def __str__(self):
        return _('Combat practice {started}').format(started=self.started)

    def get_started_date(self):
        return datetime.datetime.fromtimestamp(int(str(self.started)[:-3]))

    def get_ended_date(self):
        return datetime.datetime.fromtimestamp(int(str(self.ended)[:-3]))

    def get_winner_username(self):
        return self.winners.split(':')[1]

    def get_loser_username(self):
        return self.losers.split(':')[1]

    def get_winner_uuid(self):
        return self.winners.split(':')[0].strip()

    def get_loser_uuid(self):
        return self.losers.split(':')[0].strip()

    def get_winner_diff_elos(self):
        return int(self.winners_new_elos) - int(self.winners_old_elos)

    def get_loser_diff_elos(self):
        return int(self.losers_new_elos) - int(self.losers_old_elos)

    def get_kit_name(self):
        return self.kit_info.split(':')[0]

    def is_ranked(self):
        return 'elo=true' in self.kit_info

    class Meta:
        app_label = 'practice'
        db_table = 'fights'
        verbose_name = _('Combat Practice')
        verbose_name_plural = _('Combats Practice')

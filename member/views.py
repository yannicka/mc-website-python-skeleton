from django.contrib import messages
from django.contrib.auth import login as auth_login, logout
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _

from . import models, forms
from shop.models import PaypalPayment, DedipassPayment, PaysafecardPayment,\
    ProductPayment


def index(request):
    if request.user.is_authenticated:
        return index_logged(request)

    login_form = forms.LoginForm()
    registration_form = forms.RegistrationForm()

    if request.method == 'POST':
        # Connexion
        if 'login' in request.POST:
            login_form = forms.LoginForm(request.POST)

            if login_form.is_valid():
                email = login_form.cleaned_data.get('email')
                password = login_form.cleaned_data.get('password')

                try:
                    user = User.objects.get(email=email)

                    if not check_password(password, user.password):
                        user = None
                except User.DoesNotExist:
                    user = None

                if user is not None:
                    auth_login(request, user)
                    return redirect('signin')
                else:
                    messages.error(request, _('Compte inexistant'))
        # Inscription
        elif 'register' in request.POST:
            registration_form = forms.RegistrationForm(request.POST)

            if registration_form.is_valid():
                data = registration_form.cleaned_data

                email = registration_form.clean_email()
                password = data['password']
                password_confirm = data['password_confirm']

                # Vérification de l'égalité des mots de passe
                if password == password_confirm:
                    user = User.objects.create_user(email, email, password)
                    user.save()

                    member = models.Member()
                    member.user = user
                    member.credits = 0
                    member.save()

                    messages.success(request, _('Inscription réussie'))

                    auth_login(request, user)
                    return redirect('signin')

    # Vue
    context = {
        'title_tab': _('Connexion / Inscription'),
        'title_page': _('Connexion / Inscription'),
        'active_tab': _('login_register'),
        'login_form': login_form,
        'registration_form': registration_form,
    }

    return render(request, 'member/index.html', context)


def index_logged(request):
    current_form = ''

    if request.method == 'POST' and 'change-password' in request.POST:
        current_form = 'change-password'
        change_password_form = forms.ChangePasswordForm(request.POST)

        if change_password_form.is_valid():
            actual_password = change_password_form.cleaned_data\
                .get('actual_password')
            new_password = change_password_form.cleaned_data\
                .get('new_password')

            user = request.user

            if not check_password(actual_password, user.password):
                messages.error(request, _('Mot de passe actuel incorrect'))
            else:
                request.user.set_password(new_password)
                request.user.save()

                return redirect('index')
    else:
        change_password_form = forms.ChangePasswordForm()

    if request.method == 'POST' and 'change-email' in request.POST:
        current_form = 'change-email'
        change_email_form = forms.ChangeEmailForm(request.POST)

        if change_email_form.is_valid():
            email = change_email_form.cleaned_data['email']

            if User.objects\
               .exclude(email=request.user.email)\
               .filter(email=email).count() > 0:
                messages.error(request, _('Cette adresse électronique est '
                                          'déjà utilisée.'))
            else:
                request.user.emai = email
                request.user.save()
    else:
        change_email_form = forms.ChangeEmailForm(data={
            'email': request.user.email,
        })

    paypal_payments = PaypalPayment.objects.filter(user=request.user)\
        .order_by('-created_at')

    dedipass_payments = DedipassPayment.objects.filter(user=request.user)\
        .order_by('-created_at')

    paysafecard_payments = PaysafecardPayment.objects\
        .filter(user=request.user).order_by('-created_at')

    products_payments = ProductPayment.objects.filter(user=request.user)\
        .order_by('-created_at')

    context = {
        'title_tab': _('Mon compte'),
        'title_page': _('Mon compte'),
        'active_tab': 'my_account',
        'change_password_form': change_password_form,
        'change_email_form': change_email_form,
        'paypal_payments': paypal_payments,
        'dedipass_payments': dedipass_payments,
        'paysafecard_payments': paysafecard_payments,
        'products_payments': products_payments,
        'current_form': current_form,
    }

    return render(request, 'member/index-logged.html', context)


def logout_view(request):
    logout(request)

    return redirect('index')

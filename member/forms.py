from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class LoginForm(forms.Form):
    email = forms.EmailField(
        label=_('Adresse électronique'),
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Adresse électronique')
            }
        )
    )

    password = forms.CharField(
        label=_('Mot de passe'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Mot de passe')
            }
        )
    )


class RegistrationForm(forms.Form):
    email = forms.EmailField(
        label=_('Adresse électronique'),
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Adresse électronique')
            }
        )
    )

    password = forms.CharField(
        label=_('Mot de passe'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Mot de passe')
            }
        )
    )

    password_confirm = forms.CharField(
        label=_('Confirmation du mot de passe'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Confirmation du mot de passe')
            }
        )
    )

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if email and User.objects.filter(email=email).count():
            raise forms.ValidationError(_('L\'adresse électronique doit être '
                                          'unique.'))

        return email

    def clean_password_confirm(self):
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')

        if not password_confirm:
            raise forms.ValidationError(_('Vous devez confirmer votre mot de '
                                          'passe'))
        if password != password_confirm:
            raise forms.ValidationError(_('Les mots de passe doivent être '
                                          'identiques'))

        return password


class ChangePasswordForm(forms.Form):
    actual_password = forms.CharField(
        label=_('Mot de passe actuel'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Mot de passe actuel')
            }
        )
    )

    new_password = forms.CharField(
        label=_('Nouveau mot de passe'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Nouveau mot de passe')
            }
        )
    )

    new_password_confirm = forms.CharField(
        label=_('Confirmation du nouveau mot de passe'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Confirmation du nouveau mot de passe')
            }
        )
    )

    def clean_new_password_confirm(self):
        new_password = self.cleaned_data.get('new_password')
        new_password_confirm = self.cleaned_data.get('new_password_confirm')

        if not new_password_confirm:
            raise forms.ValidationError(_('Vous devez confirmer votre mot de '
                                          'passe'))
        if new_password != new_password_confirm:
            raise forms.ValidationError(_('Les mots de passe doivent être '
                                          'identiques'))

        return new_password


class ChangeEmailForm(forms.Form):
    email = forms.EmailField(
        label=_('Adresse électronique'),
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Adresse électronique')
            }
        )
    )

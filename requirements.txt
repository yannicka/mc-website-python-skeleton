appdirs==1.4.3
Django==2.0.7
django-ckeditor==5.2.1
django-debug-toolbar==1.7
django-wysiwyg==0.8.0
olefile==0.44
packaging==16.8
Pillow==4.0.0
pyparsing==2.2.0
requests==2.13.0
six==1.10.0
sqlparse==0.2.3
python-dateutil==2.6.0

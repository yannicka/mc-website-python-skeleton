# @see https://github.com/barneygale/MCRcon/blob/master/demo.py
# @see https://github.com/ldilley/minestat/blob/e52bb30bf9952dacf58cd6ba6459db0b54d4cd95/Python/minestat.py

# Non utilisé mais intéressant :
# - https://gist.github.com/l1am9111/720332
# - https://www.youtube.com/watch?v=hKwxzRnjEBA
# - https://pastebin.com/nDkT1pFN
# - https://pastebin.com/NWNqXcuw
# - https://pastebin.com/NZ7Ain4j

import time
import select
import socket
import struct


class RconException(Exception):
    pass


class Rcon(object):
    socket = None

    def connect(self, host, port, password):
        if self.socket is not None:
            raise RconException("Already connected")
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))
        self.send(3, password)

    def disconnect(self):
        if self.socket is None:
            raise RconException("Already disconnected")
        self.socket.close()
        self.socket = None

    def read(self, length):
        data = b""
        while len(data) < length:
            data += self.socket.recv(length - len(data))
        return data

    def send(self, out_type, out_data):
        if self.socket is None:
            raise RconException("Must connect before sending data")

        # Send a request packet
        out_payload = struct.pack('<ii', 0, out_type) + out_data.encode('utf8') + b'\x00\x00'
        out_length = struct.pack('<i', len(out_payload))
        self.socket.send(out_length + out_payload)

        # Read response packets
        in_data = ""
        while True:
            # Read a packet
            in_length, = struct.unpack('<i', self.read(4))
            in_payload = self.read(in_length)
            in_id, in_type = struct.unpack('<ii', in_payload[:8])
            in_data_partial, in_padding = in_payload[8:-2], in_payload[-2:]

            # Sanity checks
            if in_padding != b'\x00\x00':
                raise RconException("Incorrect padding")
            if in_id == -1:
                raise RconException("Login failed")

            # Record the response
            in_data += in_data_partial.decode('utf8')

            # If there's nothing more to receive, return the response
            if len(select.select([self.socket], [], [], 0)[0]) == 0:
                return in_data

    def command(self, command):
        result = self.send(2, command)
        time.sleep(0.003)  # MC-72390 workaround
        return result


class Stat:
    NUM_FIELDS = 6  # number of values expected from server

    def __init__(self, address, port, timeout=2):
        self.address = address
        self.port = port
        self.online = None       # online or offline?
        self.version = None      # server version
        self.motd = None         # message of the day
        self.nb_players = None   # current number of players online
        self.max_players = None  # maximum player capacity

        # Connect to the server and get the data
        byte_array = bytearray([0xFE, 0x01])
        raw_data = None
        data = []

        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(timeout)
            sock.connect((address, port))
            sock.settimeout(None)
            sock.send(byte_array)
            raw_data = sock.recv(512)
            sock.close()
        except:
            self.online = False

        # Parse the received data
        if raw_data is None or raw_data == '':
            self.online = False
        else:
            data = raw_data.decode('cp437').split('\x00\x00\x00')
            if data and len(data) >= self.NUM_FIELDS:
                self.online = True
                self.version = data[2]
                self.motd = data[3]
                self.nb_players = data[4]
                self.max_players = data[5]
            else:
                self.online = False

from collections import OrderedDict
from datetime import date
from dateutil.relativedelta import relativedelta

from django.db.models.aggregates import Sum
from django.db.models.functions.datetime import TruncMonth

from member.models import PracticeStats
from shop.models import PaypalPayment, DedipassPayment, PaysafecardPayment,\
    MoneyIncome, MoneyOut


class Flux:
    Paypal = 0
    Dedipass = 1
    Paysafecard = 2
    Income = 3
    Out = 4
    Profit = 5

    def flux_name(a):
        if a == Flux.Paypal:
            return 'PayPal'
        elif a == Flux.Dedipass:
            return 'Dedipass'
        elif a == Flux.Paysafecard:
            return 'Paysafecard'
        elif a == Flux.Income:
            return 'Entrée'
        elif a == Flux.Out:
            return 'Sortie'
        elif a == Flux.Profit:
            return 'Bénéfice'


def last_payments():
    amounts = OrderedDict()

    months = [date.today() + relativedelta(months=i) for i in range(-36, 1)]
    months = months[::-1]

    for month in months:
        gains_paypal = PaypalPayment.objects\
            .filter(finished=True)\
            .filter(created_at__year=month.year)\
            .filter(created_at__month=month.month)

        gain_paypal = 0

        for gain in gains_paypal:
            gain_paypal += gain.received_money

        gains_dedipass = DedipassPayment.objects\
            .filter(finished=True)\
            .filter(created_at__year=month.year)\
            .filter(created_at__month=month.month)

        gain_dedipass = 0

        for gain in gains_dedipass:
            gain_dedipass += gain.received_money

        gains_paysafecard = PaysafecardPayment.objects\
            .filter(status='A')\
            .filter(created_at__year=month.year)\
            .filter(created_at__month=month.month)

        gain_paysafecard = 0

        for gain in gains_paysafecard:
            gain_paysafecard += gain.received_money

        gains_incone = MoneyIncome.objects\
            .filter(release_date__year=month.year)\
            .filter(release_date__month=month.month)

        gain_income = 0

        for gain in gains_incone:
            gain_income += gain.amount

        gains_out = MoneyOut.objects\
            .filter(release_date__year=month.year)\
            .filter(release_date__month=month.month)

        gain_out = 0

        for gain in gains_out:
            gain_out += gain.amount

        gain_out = -gain_out

        gain_profit =\
            gain_out +\
            gain_paypal +\
            gain_dedipass +\
            gain_paysafecard +\
            gain_income

        amounts[month] = OrderedDict()
        amounts[month][Flux.Paypal] = gain_paypal
        amounts[month][Flux.Dedipass] = gain_dedipass
        amounts[month][Flux.Paysafecard] = gain_paysafecard
        amounts[month][Flux.Income] = gain_income
        amounts[month][Flux.Out] = gain_out
        amounts[month][Flux.Profit] = gain_profit

    return amounts


def get_elo(name):
    return PracticeStats.objects\
        .all()\
        .extra(select={'score': name})\
        .order_by('-' + name)[:10]

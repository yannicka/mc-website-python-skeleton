from django import forms
from django.utils.translation import ugettext_lazy as _


class ContactForm(forms.Form):
    email = forms.EmailField(
        label=_('Adresse électronique'),
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Adresse électronique'),
            }
        )
    )

    subject = forms.CharField(
        label=_('Sujet'),
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Sujet'),
            }
        )
    )

    minecraft_username = forms.CharField(
        label=_('Pseudonyme Minecraft'),
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Pseudonyme Minecraft'),
            }
        )
    )

    message = forms.CharField(
        label=_('Message'),
        min_length=100,
        max_length=12000,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'placeholder': _('Message'),
            }
        )
    )


class RecruitmentForm(forms.Form):
    email = forms.EmailField(
        label=_('Adresse électronique'),
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Adresse électronique'),
            }
        )
    )

    message = forms.CharField(
        label=_('Message'),
        required=True,
        min_length=15,
        max_length=12000,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'placeholder': _('Message'),
            }
        )
    )

    file = forms.FileField(
        label=_('Fichier joint'),
        required=False,
        help_text=_('Le fichier joint est facultatif. Il est utile dans le '
                    'cas où vous souhaitez inclure une candidature avec une '
                    'mise en forme (image, couleur...).'),
    )

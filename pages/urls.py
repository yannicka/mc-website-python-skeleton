from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^confirm-contact/$', views.confirm_contact, name='confirm-contact'),
    url(r'^donate/$', views.donate, name='donate'),
    url(r'^ranking/$', views.ranking, name='ranking'),
    url(r'^terms-of-sales/$', views.terms_of_sales, name='terms-of-sales'),
    url(r'^legal-notice/$', views.legal_notice, name='legal-notice'),
    url(r'^summary/$', views.summary, name='summary'),
    url(r'^about/$', views.about, name='about'),
    url(r'^staff/$', views.staff, name='staff'),
    url(r'^youtubers/$', views.youtubers, name='youtubers'),
    url(r'^page/(?P<slug>[\w-]+)/$', views.page, name='page'),
    url(r'^recruitments/$', views.recruitments, name='recruitments'),
    url(r'^recruitment/(?P<slug>[\w-]+)/$', views.recruitment,
        name='recruitment'),
    url(r'^confirm-recruitment/$', views.confirm_recruitment,
        name='confirm-recruitment'),
    url(r'^fight/(?P<started>[\w-]+)/$', views.fight, name='fight'),
]

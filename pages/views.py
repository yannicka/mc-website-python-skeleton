import re

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail.message import EmailMessage
from django.db.models.aggregates import Sum
from django.shortcuts import render, render_to_response, get_object_or_404
from django.utils.translation import gettext as _
from django.shortcuts import redirect
from django.template.loader import render_to_string

from shop.models import PaypalPayment, DedipassPayment, PaysafecardPayment,\
    MoneyIncome, MoneyOut
from website.models import Youtuber, StaffGroup, Page, Recruitment,\
    RecruitmentRequest, Contact
from . import forms
from .utils import last_payments, get_elo
from website.settings import EMAIL_CONTACT, SITE_NAME
from member.models import PracticeFight


def index(request):
    context = {
        'override_title_tab': _('%(site_name)s - Serveur Minecraft Network '
                                '1.7  à 1.13') % { 'site_name': SITE_NAME },
        'title_page': _('Home'),
        'description': _('%(site_name)s - Serveur Minecraft Network 1.7 '
                         'à 1.13 - Practice, FFA & Faction') %
                            { 'site_name': SITE_NAME },
        'active_tab': 'home',
        'full_width': True,
        'body_classes': ['home'],
        'is_homepage': True,
    }

    return render(request, 'pages/index.html', context)


def contact(request):
    if request.method == 'POST':
        contact_form = forms.ContactForm(request.POST)

        if contact_form.is_valid():
            email = contact_form.cleaned_data.get('email')
            subject = contact_form.cleaned_data.get('subject')
            minecraft_username = contact_form.cleaned_data\
                .get('minecraft_username')
            message = contact_form.cleaned_data.get('message')

            contact = Contact()
            contact.email = email
            contact.subject = subject
            contact.minecraft_username = minecraft_username
            contact.message = message
            contact.user = request.user
            contact.save()

            return redirect('confirm-contact')
    else:
        email = ''
        is_logged = request.user.is_authenticated()

        if is_logged:
            email = request.user.email

        contact_form = forms.ContactForm(initial={'email': email})

        contact_form.fields['email'].widget.attrs['readonly'] = is_logged

    context = {
        'title_tab': _('Contactez-nous'),
        'title_page': _('Formulaire de contact'),
        'active_tab': 'contact',
        'contact_form': contact_form,
    }

    return render(request, 'pages/contact.html', context)


def confirm_contact(request):
    context = {
        'title_tab': _('Confirmation de contact'),
        'title_page': _('Confirmation de contact'),
        'active_tab': 'contact',
    }

    return render(request, 'pages/confirm-contact.html', context)


def donate(request):
    context = {
        'title_tab': _('Donation'),
        'title_page': _('Donation'),
        'active_tab': 'donate',
    }

    return render(request, 'pages/donate.html', context)


def ranking(request):
    global_elo = get_elo('global_elo')

    elo_nodebuffelo1v1 = get_elo('elo_nodebuffelo1v1')
    elo_builduhcelo1v1 = get_elo('elo_builduhcelo1v1')
    elo_archerelo1v1 = get_elo('elo_archerelo1v1')
    elo_gappleelo1v1 = get_elo('elo_gappleelo1v1')
    elo_comboelo1v1 = get_elo('elo_comboelo1v1')
    elo_combosumoelo1v1 = get_elo('elo_combosumoelo1v1')
    elo_sumoelo1v1 = get_elo('elo_sumoelo1v1')
    elo_spleefelo1v1 = get_elo('elo_spleefelo1v1')

    elo_nodebuffelo1v1premium = get_elo('elo_nodebuffelo1v1premium')
    elo_builduhcelo1v1premium = get_elo('elo_builduhcelo1v1premium')
    elo_archerelo1v1premium = get_elo('elo_archerelo1v1premium')
    elo_gappleelo1v1premium = get_elo('elo_gappleelo1v1premium')
    elo_comboelo1v1premium = get_elo('elo_comboelo1v1premium')
    elo_combosumoelo1v1premium = get_elo('elo_combosumoelo1v1premium')
    elo_sumoelo1v1premium = get_elo('elo_sumoelo1v1premium')
    elo_spleefelo1v1premium = get_elo('elo_spleefelo1v1premium')

    context = {
        'title_tab': _('Classement'),
        'title_page': _('Classement'),
        'active_tab': 'ranking',

        'global_elo': global_elo,

        'elo_nodebuffelo1v1': elo_nodebuffelo1v1,
        'elo_builduhcelo1v1': elo_builduhcelo1v1,
        'elo_archerelo1v1': elo_archerelo1v1,
        'elo_gappleelo1v1': elo_gappleelo1v1,
        'elo_comboelo1v1': elo_comboelo1v1,
        'elo_combosumoelo1v1': elo_combosumoelo1v1,
        'elo_sumoelo1v1': elo_sumoelo1v1,
        'elo_spleefelo1v1': elo_spleefelo1v1,

        'elo_nodebuffelo1v1premium': elo_nodebuffelo1v1premium,
        'elo_builduhcelo1v1premium': elo_builduhcelo1v1premium,
        'elo_archerelo1v1premium': elo_archerelo1v1premium,
        'elo_gappleelo1v1premium': elo_gappleelo1v1premium,
        'elo_comboelo1v1premium': elo_comboelo1v1premium,
        'elo_combosumoelo1v1premium': elo_combosumoelo1v1premium,
        'elo_sumoelo1v1premium': elo_sumoelo1v1premium,
        'elo_spleefelo1v1premium': elo_spleefelo1v1premium,
    }

    return render(request, 'pages/ranking.html', context)


def fight(request, started):
    fight = get_object_or_404(PracticeFight, started=started)

    context = {
        'title_tab': _('Combat'),
        'title_page': _('Combat'),
        'active_tab': 'fight',
        'fight': fight,
    }

    return render(request, 'pages/fight.html', context)


def terms_of_sales(request):
    context = {
        'title_tab': _('Conditions générales de vente'),
        'title_page': _('Conditions générales de vente'),
        'active_tab': 'terms_of_sales',
    }

    return render(request, 'pages/terms-of-sales.html', context)


def legal_notice(request):
    context = {
        'title_tab': _('Mentions légales'),
        'title_page': _('Mentions légales'),
        'active_tab': 'legal_notice',
    }

    return render(request, 'pages/legal-notice.html', context)


@staff_member_required
def summary(request):
    fluxes = last_payments()

    headings = []

    for month, flux in fluxes.items():
        for heading in flux:
            headings.append(heading)

        break

    gain_paypal = PaypalPayment.objects.filter(finished=True)\
        .aggregate(Sum('received_money')).get('received_money__sum')

    if gain_paypal is None:
        gain_paypal = 0

    gain_dedipass = DedipassPayment.objects.filter(finished=True)\
        .aggregate(Sum('received_money')).get('received_money__sum')

    if gain_dedipass is None:
        gain_dedipass = 0

    gain_paysafecard = PaysafecardPayment.objects.filter(status='A')\
        .aggregate(Sum('received_money')).get('received_money__sum')

    if gain_paysafecard is None:
        gain_paysafecard = 0

    income = MoneyIncome.objects\
        .aggregate(Sum('amount')).get('amount__sum')

    if income is None:
        income = 0

    output = MoneyOut.objects\
        .aggregate(Sum('amount')).get('amount__sum')

    if output is None:
        output = 0

    total = gain_paypal + gain_dedipass + gain_paysafecard + income - output

    context = {
        'title_tab': _('Résumé'),
        'title_page': _('Résumé'),
        'active_tab': 'summary',
        'gain_paypal': gain_paypal,
        'gain_dedipass': gain_dedipass,
        'gain_paysafecard': gain_paysafecard,
        'income': income,
        'output': output,
        'total': total,
        'fluxes': fluxes,
        'headings': headings,
    }

    return render(request, 'pages/summary.html', context)


def about(request):
    context = {
        'title_tab': _('À propos'),
        'title_page': _('À propos'),
        'active_tab': 'about',
    }

    return render(request, 'pages/about.html', context)


def staff(request):
    groups = StaffGroup.objects.all()

    context = {
        'title_tab': _('Équipe'),
        'title_page': _('Équipe'),
        'active_tab': 'staff',
        'groups': groups,
    }

    return render(request, 'pages/staff.html', context)


def youtubers(request):
    youtubers = Youtuber.objects.all().order_by('username')

    context = {
        'title_tab': _('Youtubeurs'),
        'title_page': _('Youtubeurs'),
        'active_tab': 'youtubers',
        'youtubers': youtubers,
    }

    return render(request, 'pages/youtubers.html', context)


def page(request, slug):
    page = get_object_or_404(Page, slug=slug)

    context = {
        'title_tab': page.name,
        'title_page': page.name,
        'page': page,
    }

    return render(request, 'pages/page.html', context)


def recruitments(request):
    recruitments = Recruitment.objects.all()

    context = {
        'title_tab': _('Recrutements'),
        'title_page': _('Recrutements'),
        'active_tab': 'recruitments',
        'recruitments': recruitments,
    }

    return render(request, 'pages/recruitments.html', context)


@login_required
def recruitment(request, slug):
    recruitment = get_object_or_404(Recruitment, slug=slug, opened=True)

    if request.method == 'POST':
        recruitment_form = forms.RecruitmentForm(request.POST,  request.FILES)

        if recruitment_form.is_valid():
            email = recruitment_form.cleaned_data.get('email')
            message = recruitment_form.cleaned_data.get('message')

            recruitment_request = RecruitmentRequest()
            recruitment_request.email = email
            recruitment_request.message = message
            recruitment_request.file = request.FILES.get('file', None)
            recruitment_request.user = request.user
            recruitment_request.recruitment = recruitment
            recruitment_request.save()

            messages.success(request, _('Message envoyé'))

            return redirect('confirm-recruitment')  # todo
    else:
        email = ''
        is_logged = request.user.is_authenticated()

        if is_logged:
            email = request.user.email

        recruitment_form = forms.RecruitmentForm(initial={'email': email})

        recruitment_form.fields['email'].widget.attrs['readonly'] = is_logged

    context = {
        'title_tab': _('Recrutement de « {name} »').format(
            name=recruitment.name),
        'title_page': _('Recrutement de « {name} »').format(
            name=recruitment.name),
        'active_tab': 'recruitments',
        'recruitment': recruitment,
        'recruitment_form': recruitment_form,
    }

    return render(request, 'pages/recruitment.html', context)


def confirm_recruitment(request):
    context = {
        'title_tab': _('Confirmation de demande de recrutement'),
        'title_page': _('Confirmation de demande de recrutement'),
        'active_tab': 'recruitments',
    }

    return render(request, 'pages/confirm-recruitment.html', context)


def handler404(request):
    context = {
        'title_tab': _('Erreur 404'),
        'title_page': _('Erreur 404'),
    }

    response = render_to_response('pages/404.html',
                                  context=context)
    response.status_code = 404

    return response


def handler500(request):
    context = {
        'title_tab': _('Erreur 500'),
        'title_page': _('Erreur 500'),
    }

    response = render_to_response('pages/500.html',
                                  context=context)
    response.status_code = 500

    return response

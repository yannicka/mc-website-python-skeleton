from django import template

from pages.utils import Flux


def flux_name(value):
    return Flux.flux_name(value)


register = template.Library()
register.filter('flux_name', flux_name)

# Generated by Django 2.0.7 on 2018-10-02 18:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Config',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('global_promotion', models.DecimalField(decimal_places=2, max_digits=6, verbose_name='Promotion globale')),
            ],
            options={
                'verbose_name': 'Configuration',
                'verbose_name_plural': 'Configurations',
            },
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, verbose_name='Adresse électronique')),
                ('subject', models.CharField(max_length=255, verbose_name='Sujet')),
                ('minecraft_username', models.CharField(max_length=150, verbose_name='Pseudonyme Minecraft')),
                ('message', models.TextField(verbose_name='Message')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Créé le')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Mis à jour le')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Utilisateur')),
            ],
            options={
                'verbose_name': 'Contact',
                'verbose_name_plural': 'Contacts',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=128, verbose_name='Nom')),
                ('content', models.TextField(blank=True, verbose_name='Contenu')),
                ('slug', models.SlugField(max_length=128, unique=True, verbose_name='Slug')),
            ],
            options={
                'verbose_name': 'Page',
                'verbose_name_plural': 'Pages',
            },
        ),
        migrations.CreateModel(
            name='Recruitment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='Nom')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                ('image', models.ImageField(blank=True, upload_to='./', verbose_name='Image')),
                ('opened', models.BooleanField(default=True, verbose_name='Ouvert')),
                ('slug', models.SlugField(max_length=128, unique=True, verbose_name='Slug')),
                ('position', models.IntegerField(default=0, verbose_name='Position')),
                ('qualifications', models.TextField(blank=True, verbose_name='Qualifications')),
            ],
            options={
                'verbose_name': 'Recrutement',
                'verbose_name_plural': 'Recrutements',
            },
        ),
        migrations.CreateModel(
            name='RecruitmentRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, verbose_name='Adresse électronique')),
                ('message', models.TextField(blank=True, verbose_name='Message')),
                ('file', models.ImageField(blank=True, upload_to='./', verbose_name='Fichier')),
                ('status', models.CharField(choices=[('TO_STUDY', 'À étudier'), ('ACCEPTED', 'Acceptée'), ('REFUSED', 'Refusée')], default='TO_STUDY', max_length=8, verbose_name='Statut')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Créé le')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Mis à jour le')),
                ('recruitment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='website.Recruitment', verbose_name='Recrutement')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Utilisateur')),
            ],
            options={
                'verbose_name': 'Demande de recrutement',
                'verbose_name_plural': 'Demandes de recrutement',
            },
        ),
        migrations.CreateModel(
            name='StaffGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Nom')),
                ('position', models.IntegerField(default=0, verbose_name='Position')),
            ],
            options={
                'verbose_name': 'Groupe',
                'verbose_name_plural': 'Groupes',
                'ordering': ('position',),
            },
        ),
        migrations.CreateModel(
            name='StaffMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, verbose_name='Nom')),
                ('username', models.CharField(blank=True, max_length=50, verbose_name="Nom d'utilisateur")),
                ('uuid', models.CharField(blank=True, max_length=36, verbose_name='UUID')),
                ('short_description', models.CharField(blank=True, max_length=128, verbose_name='Description courte')),
                ('position', models.IntegerField(default=0, verbose_name='Position')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='website.StaffGroup', verbose_name='Groupe')),
            ],
            options={
                'verbose_name': 'Membre',
                'verbose_name_plural': 'Membres',
                'ordering': ('position',),
            },
        ),
        migrations.CreateModel(
            name='Youtuber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=50, unique=True, verbose_name="Nom d'utilisateur")),
                ('uuid', models.CharField(blank=True, max_length=36, unique=True, verbose_name='UUID')),
                ('url_youtube', models.CharField(blank=True, max_length=255, verbose_name='URL YouTube')),
            ],
            options={
                'verbose_name': 'Youtubeur',
                'verbose_name_plural': 'Youtubeurs',
            },
        ),
    ]

import requests

from django.contrib import admin

from .models import Config, Youtuber, StaffGroup, StaffMember, Page,\
    Recruitment, RecruitmentRequest, Contact
from shop.utils import get_uuid_from_minecraft_username


@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
    pass


@admin.register(Youtuber)
class YoutuberAdmin(admin.ModelAdmin):
    list_display = ('username', 'uuid', 'url_youtube')

    def save_model(self, request, obj, form, change):
        obj.uuid = get_uuid_from_minecraft_username(obj.username)
        obj.save()


@admin.register(StaffGroup)
class StaffGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'position')
    list_editable = ('position',)


@admin.register(StaffMember)
class StaffMemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'username', 'uuid', 'short_description', 'group',
                    'position')
    list_editable = ('position',)
    list_filter = ('group__name',)

    def save_model(self, request, obj, form, change):
        if 'username' in form.changed_data:
            obj.uuid = get_uuid_from_minecraft_username(obj.username)

        obj.save()


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}
    change_form_template = 'admin/website/page-form.html'


@admin.register(Recruitment)
class RecruitmentAdmin(admin.ModelAdmin):
    list_display = ('name',  'slug', 'position', 'opened')
    prepopulated_fields = {'slug': ('name',)}
    list_editable = ('position', 'opened',)
    change_form_template = 'admin/website/recruitment-form.html'


@admin.register(RecruitmentRequest)
class RecruitmentRequestAdmin(admin.ModelAdmin):
    list_display = ('email', 'recruitment_name', 'file', 'user', 'status',
                    'created_at')
    list_filter = ('recruitment__name', 'status')
    ordering = ('-created_at',)

    def recruitment_name(self, obj):
        return obj.recruitment.name
    recruitment_name.short_description = 'Nom du recrutement'
    recruitment_name.admin_order_field = 'recruitment_name'


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('subject', 'email', 'minecraft_username', 'user',
                    'created_at')
    ordering = ('-created_at',)

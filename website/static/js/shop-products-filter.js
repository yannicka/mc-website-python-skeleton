(function() {
    // Liste des produits
    var productElements = document.querySelectorAll('.list-products > li');
    productElements = [].slice.call(productElements);

    // Liste des catégories
    var categoryElements = document.querySelectorAll('.list-categories > li');
    categoryElements = [].slice.call(categoryElements);

    function filterProducts(el) {
        // Récupération du slug
        var slug = el.getAttribute('data-slug');

        // Modification de l'URL
        window.location.hash = slug;

        // Suppression de la classe « active » à toutes les catégories
        for (var i in categoryElements)
            if (categoryElements.hasOwnProperty(i))
                categoryElements[i].classList.remove('active');

        // Ajout de la classe « active » à la catégorie courante
        el.classList.add('active');

        // Cache tous les produits sauf ceux de la catégorie sélectionnée
        // (exception : « all » qui affiche tous les produits)
        for (i in productElements) {
            if (productElements.hasOwnProperty(i)) {
                var productElement = productElements[i];

                if (productElement.getAttribute('data-category-slug') == slug ||
                    slug == 'all')
                    productElement.style.display = 'inline-block';
                else
                    productElement.style.display = 'none';
            }
        }
    }

    // Clic sur une catégorie : filtre les produits de cette catégorie
    for (var i = 0, l = categoryElements.length ; i < l ; i++) {
        var categoryElement = categoryElements[i];

        categoryElement.addEventListener('click', (function() {
            return function() {
                filterProducts(this)
            };
        })());
    }

    // Filtre selon le slug de l'URL au chargement de la page
    var filter = (window.location.hash || '').substr(1);
    var currentCategory = document
        .querySelector('[data-slug="' + filter + '"]');

    if (currentCategory)
        filterProducts(currentCategory);
})();

(function() {
    var lightboxLinks = document.querySelectorAll('.lightbox-link');
    lightboxLinks = Array.prototype.slice.call(lightboxLinks);

    /*
    for (var i = 0, l = lightboxLinks.length ; i < l ; i++) {
        var lightboxLink = lightboxLinks[i];

        lightboxLink.addEventListener('click', function() {
            var lightbox = document.querySelector(
                '#' + this.getAttribute('data-lightbox'));

            lightbox.style.display = 'block';
        });
    }
    */

    document.addEventListener('click', function(e) {
        for (var i = 0, l = lightboxLinks.length ; i < l ; i++) {
            var lightboxLink = lightboxLinks[i];

            var lightbox = document.querySelector(
                '[data-lightbox="' + lightboxLink.getAttribute('data-lightbox-link') + '"]');

            var target = e.target;
            var isLightbox = false;
            var isFirstPass = true;

            do {
                if (target == lightboxLink && isFirstPass) {
                    if (lightbox.style.display == 'block')
                        lightbox.style.display = 'none';
                    else
                        lightbox.style.display = 'block';

                    isLightbox = true;

                    break;
                }

                if (target == lightbox) {
                    lightbox.style.display = 'block';
                    isLightbox = true;

                    break;
                }

                isFirstPass = false;
            } while ((target = target.parentNode) && target != null);

            if (!isLightbox)
                lightbox.style.display = 'none';
        }
    });
})();

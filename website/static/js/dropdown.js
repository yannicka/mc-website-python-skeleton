(function() {
    var dropdowns = document.querySelectorAll('.dropdown');

    for (var i = 0, l = dropdowns.length ; i < l ; i++) {
        var dropdown = dropdowns[i];

        var menu = dropdown.querySelector('.dropdown-menu');

        menu.style.display = 'none';
    }

    // cf. http://stackoverflow.com/a/18162093/1974228
    function childOf(c, p){
        while ((c = c.parentNode) && c !== p) { }

        return !!c;
    }

    document.addEventListener('click', function(e) {
        for (var i = 0, l = dropdowns.length ; i < l ; i++) {
            var dropdown = dropdowns[i];
            var toggle = dropdown.querySelector('.dropdown-toggle');
            var menu = dropdown.querySelector('.dropdown-menu');
            var target = e.target;
            var isDropdown = false;
            var isFirstPass = true;

            do {
                if ((target == toggle || childOf(target, toggle)) && isFirstPass) {
                    if (menu.style.display == 'block')
                        menu.style.display = 'none';
                    else
                        menu.style.display = 'block';

                    isDropdown = true;

                    break;
                }

                if (target == dropdown) {
                    menu.style.display = 'block';
                    isDropdown = true;

                    break;
                }

                isFirstPass = false;
            } while ((target = target.parentNode) && target != null);

            if (!isDropdown)
                menu.style.display = 'none';
        }
    });
})();

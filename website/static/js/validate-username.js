(function() {
    var form = document.querySelector('.validate-username-form');
    var formValidateBuyItem = document.querySelector('.validate-buy-item');

    formValidateBuyItem.style.display = 'none';

    form.addEventListener('submit', function(e) {
        e.preventDefault();

        var minecraftUsername =
            document.querySelector('[name="minecraft-username"]').value;

        var serverSlug =
            document.querySelector('[name="server-slug"]').value;

        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                if (xhr.response == '1') {
                    form.style.display = 'none';

                    formValidateBuyItem.style.display = 'block';

                    var textMinecraftUsername =
                        document.querySelector('.text-minecraft-username');

                    var inputMinecraftUsername =
                        document.querySelector('.input-minecraft-username');

                    inputMinecraftUsername.value = minecraftUsername;
                    textMinecraftUsername.innerHTML = minecraftUsername;
                } else {
                    alert('Pseudonyme invalide');
                }
            }
        };

        xhr.open('GET', '/shop/validate-username/'
            + serverSlug + '/'
            + minecraftUsername, true);

        xhr.send(null);

        return false;
    });
})();

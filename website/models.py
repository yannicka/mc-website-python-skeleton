from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Config(models.Model):
    """
    Configuration du site.
    """
    global_promotion = models.DecimalField(_('Promotion globale'),
                                           max_digits=6, decimal_places=2)

    def __str__(self):
        return _('Configuration').format()

    class Meta:
        verbose_name = _('Configuration')
        verbose_name_plural = _('Configurations')


class Youtuber(models.Model):
    """
    Un « YouTubeur » est un vidéaste qui publie des vidéos sur YouTube
    concernant notre serveur.
    """
    username = models.CharField(_('Nom d\'utilisateur'), max_length=50,
                                unique=True)
    uuid = models.CharField(_('UUID'), max_length=36, unique=True, blank=True)
    url_youtube = models.CharField(_('URL YouTube'), max_length=255,
                                   blank=True)

    def __str__(self):
        return _('Youtubeur « {username} »').format(username=self.username)

    class Meta:
        verbose_name = _('Youtubeur')
        verbose_name_plural = _('Youtubeurs')


class StaffGroup(models.Model):
    name = models.CharField(_('Nom'), max_length=64)
    position = models.IntegerField(_('Position'), default=0)

    def __str__(self):
        return _('Groupe d\'équipe « {name} »').format(name=self.name)

    class Meta:
        verbose_name = _('Groupe')
        verbose_name_plural = _('Groupes')
        ordering = ('position',)


class StaffMember(models.Model):
    name = models.CharField(_('Nom'), max_length=50, blank=True)
    username = models.CharField(_('Nom d\'utilisateur'), max_length=50,
                                blank=True)
    uuid = models.CharField(_('UUID'), max_length=36, blank=True)
    short_description = models.CharField(_('Description courte'),
                                         max_length=128, blank=True)
    group = models.ForeignKey(StaffGroup, on_delete=models.CASCADE,
                              verbose_name=_('Groupe'))
    position = models.IntegerField(_('Position'), default=0)

    def __str__(self):
        return _('Membre d\'équipe « {username} »').format(
            username=self.username)

    class Meta:
        verbose_name = _('Membre')
        verbose_name_plural = _('Membres')
        ordering = ('position',)


class Page(models.Model):
    name = models.CharField(_('Nom'), max_length=128, blank=True)
    content = models.TextField(_('Contenu'), blank=True)
    slug = models.SlugField(_('Slug'), max_length=128, unique=True)

    def __str__(self):
        return _('Page « {name} »').format(name=self.name)

    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')


class Recruitment(models.Model):
    name = models.CharField(_('Nom'), max_length=128)
    description = models.TextField(_('Description'), blank=True)
    image = models.ImageField(_('Image'), upload_to='./', blank=True)
    opened = models.BooleanField(_('Ouvert'), default=True)
    slug = models.SlugField(_('Slug'), max_length=128, unique=True)
    position = models.IntegerField(_('Position'), default=0)
    qualifications = models.TextField(_('Qualifications'), blank=True)

    def __str__(self):
        return _('Recrutement « {name} »').format(name=self.name)

    class Meta:
        verbose_name = _('Recrutement')
        verbose_name_plural = _('Recrutements')


class RecruitmentRequest(models.Model):
    STATUSES = (
        ('TO_STUDY', _('À étudier')),
        ('ACCEPTED', _('Acceptée')),
        ('REFUSED', _('Refusée'))
    )

    email = models.EmailField(_('Adresse électronique'))
    message = models.TextField(_('Message'), blank=True)
    file = models.ImageField(_('Fichier'), upload_to='./', blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             verbose_name=_('Utilisateur'))
    recruitment = models.ForeignKey(Recruitment, on_delete=models.CASCADE,
                                    verbose_name=_('Recrutement'))
    status = models.CharField(_('Statut'), max_length=8, choices=STATUSES,
                              default='TO_STUDY')
    created_at = models.DateTimeField(_('Créé le'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Mis à jour le'), auto_now=True)

    def __str__(self):
        return _('Demande de recrutement « {recruitment_name} »').format(
            recruitment_name=self.recruitment.name)

    class Meta:
        verbose_name = _('Demande de recrutement')
        verbose_name_plural = _('Demandes de recrutement')


class Contact(models.Model):
    email = models.EmailField(_('Adresse électronique'))
    subject = models.CharField(_('Sujet'), max_length=255)
    minecraft_username = models.CharField(_('Pseudonyme Minecraft'),
                                          max_length=150)
    message = models.TextField(_('Message'))
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             verbose_name=_('Utilisateur'))
    created_at = models.DateTimeField(_('Créé le'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Mis à jour le'), auto_now=True)

    def __str__(self):
        return _('Contact « {subject} »').format(subject=self.subject)

    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')

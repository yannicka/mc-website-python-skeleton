from datetime import datetime, timedelta
import os

from .settings import BASE_URL, SITE_VERSION, SITE_NAME, SITE_AUTHORS,\
    SOCIAL_TWITTER_URL, SOCIAL_TEAMSPEAK_URL, SOCIAL_DISCORD_URL,\
    MINECRAFT_IP, DONATION_URL
from minecraft.rcon import Stat
from shop.models import Server


def extra_context(request):
    nb_players = 0

    dir_path = os.path.dirname(os.path.realpath(__file__ + '/../'))
    filename = dir_path + '/nb_players.txt'

    # Si le fichier contenant le nombre de joueur existe,
    #
    # on récupère la date courante et la date de dernière modification du
    # fichier,
    #
    # puis on regarde si ça fait plus ou moins une minute :
    # - si le fichier a moins d'une minute, il est lu ;
    # - sinon, le nombre de joueur est calculé et enregistré.
    if os.path.isfile(filename):
        current_date = datetime.now()
        mtime_file = datetime.fromtimestamp(os.stat(filename).st_mtime)

        if (current_date - mtime_file) < timedelta(minutes=1):
            with open(filename, 'r') as f:
                nb_players = f.read()
        else:
            nb_players = get_nb_players()

            with open(filename, 'w') as f:
                f.write(str(nb_players))
    # Si le fichier n'existe pas, le nombre de joueur est calculé et
    # enregistré.
    else:
        nb_players = get_nb_players()

        with open(filename, 'w') as f:
            f.write(str(nb_players))

    return {
        'base_url': BASE_URL,
        'site_version': SITE_VERSION,
        'site_name': SITE_NAME,
        'site_authors': SITE_AUTHORS,
        'social_twitter_url': SOCIAL_TWITTER_URL,
        'social_teamspeak_url': SOCIAL_TEAMSPEAK_URL,
        'social_discord_url': SOCIAL_DISCORD_URL,
        'minecraft_ip': MINECRAFT_IP,
        'donation_url': DONATION_URL,
        'nb_players': nb_players,
    }


def get_nb_players():
    nb_players = 0

    proxy = Server.get_proxy()

    if proxy:
        stat = Stat(proxy.ip, proxy.port)

        nb_players = stat.nb_players

        return nb_players
    else:
        return 0

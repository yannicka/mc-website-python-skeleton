# mc-website-python-skeleton

Code source du projet : https://gitlab.com/yannicka/mc-website-python-skeleton

Langage de programmation : Python 3.5 (framework Django version 2.0).

Ce projet et tous les fichiers qui le compose sont distribués sous la licence
AGPLv3 (voir [LICENSE.txt](LICENSE.txt)).

## Lancer le serveur en local

```bash
python3 manage.py runserver
```

## Mise en production

```bash
mkdir /var/www/html/site
mkdir /var/www/html/static
mkdir /var/www/html/uploads
nano /etc/apache2/sites-available/mysite.fr.conf # contenu ci-après
nano /etc/apache2/sites-available/mysite.fr.tls.conf # contenu ci-après
a2ensite mysite.fr
a2ensite mysite.fr.tls
cd DOSSIER_SITE
pip3 install -r requirements.txt
make css
python3 manage.py collectstatic
service apache2 restart
```

Pensez à bien activer rcon sur votre serveur Minecraft car c'est par ce biais
que communique le site avec le serveur Minecraft.

### mysite.fr.conf

```apache
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    ServerName mysite.fr
    ServerAlias www.mysite.fr

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    # HTTP -> HTTPS
    RewriteEngine on
    RewriteCond %{HTTPS} off
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [NE,L,R=301]
</VirtualHost>
```

### mysite.fr.tls.conf

```apache
WSGISocketPrefix /var/run/wsgi

<VirtualHost *:443>
    ServerAdmin webmaster@localhost
    ServerName mysite.fr
    ServerAlias www.mysite.fr

    # www -> non-www
    RewriteEngine on
    RewriteCond %{HTTP_HOST} ^www\.(.*)$ [NC]
    RewriteRule ^(.*)$ http://%1/$1 [R=301,L]

    # RewriteCond %{REMOTE_ADDR} !(IP_AUTORISE)
    # RewriteCond %{REQUEST_URI} !.(jpe?g|png|gif|ico) [NC]
    # RewriteRule .* ./static/maintenance.html [R=302,L]

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    Alias /static /var/www/html/site/static
    <Directory /var/www/html/site/static>
        Require all granted
        Options -Indexes
    </Directory>

    Alias /uploads /var/www/html/site/uploads
    <Directory /var/www/html/site/uploads>
            Require all granted
    Options -Indexes
    </Directory>

    <Directory /home/user/site/mysite>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIApplicationGroup %{GLOBAL}
    WSGIDaemonProcess mysite python-path=/home/user/site:/root/.virtualenvs/venv/lib/python3.4/site-packages

    WSGIProcessGroup mysite
    WSGIScriptAlias / /home/user/site/mysite/wsgi.py

    SSLProtocol -ALL +TLSv1.2
    #SSLHonorCipherOrder On
    #SSLCipherSuite ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS

    #SSLProtocol all -SSLv2 -SSLv3
    SSLHonorCipherOrder on
    SSLCipherSuite "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS !RC4"

    SSLEngine on
    SSLCertificateFile /etc/letsencrypt/live/mysite.fr/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/mysite.fr/privkey.pem

    <IfModule mod_expires.c>
        ExpiresActive On
        ExpiresDefault "access plus 1 month"
        ExpiresByType image/x-icon "access plus 1 year"
        ExpiresByType image/gif "access plus 1 month"
        ExpiresByType image/png "access plus 1 month"
        ExpiresByType image/jpeg "access plus 1 month"
        ExpiresByType text/css "access plus 1 year"
        ExpiresByType application/javascript "access plus 1 year"
    </IfModule>

    <IfModule mod_headers.c>
        Header always set X-Content-Type-Options "nosniff"
    </IfModule>
</VirtualHost>
```

## mysite/local_settings.py

```python
# coding: utf-8

from __future__ import unicode_literals


# URL

BASE_URL = 'https://mysite.fr'


# HTTPS

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True


# Base de données

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'db_password',
        'HOST': 'db_host',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        }
    },

    'witrush_db': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'db_password',
        'HOST': 'db_host',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        }
    },

    'practice_db': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'db_password',
        'HOST': 'db_host',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        }
    }
}


# Clé secrète

SECRET_KEY = '--------------------------------------------------'


# Débogue

DEBUG = False

ALLOWED_HOSTS = ['*']


# Chemins

STATIC_ROOT = '/var/www/html/site/static/'
MEDIA_ROOT = '/var/www/html/site/uploads/'


# Dedipass

DEDIPASS_PUBLIC_KEY = '-'
DEDIPASS_PRIVATE_KEY = '-'


# PayPal

PAYPAL_TEST = False
PAYPAL_RECEIVER_EMAIL = '-'


# Email

EMAIL_HOST = '-'
EMAIL_HOST_USER = '-'
EMAIL_HOST_PASSWORD = str('-')
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_CONTACT = '-'

DEFAULT_FROM_EMAIL = '-'
```

## Modes de paiement

### paysafecard

Rien à configurer. L'utilisateur fait une saisie, et elle est enregistrée sur
le site. Aucune notification n'avertie de cette saisie. Il faut ensuite
récupérer le code saisie puis l'intégrer dans paysafecard pour recevoir
l'argent puis appliquer manuellement le gain à l'utilisateur.

Une évolution possible serait d'utiliser l'API de paysafecard.

### PayPal

- Créez un compte PayPal ;
- Activez les « Notification instantanée de paiement (IPN) » ;
- URL de notification : https://mysite.fr/shop/validate-credits-paypal/.

### Dedipass

- Créez un compte ;
- Créez un service :
    - Nom : Monnaie virtuelle (crédits) ;
    - Type : monnaie virtuelle ;
    - Site : https://mysite.fr/ ;
    - URL de redirection : https://mysite.fr/shop/validate-credits-dedipass/ ;
    - Le reste comme vous le souhaitez ;
- Modifier le fichier de configuration du site :
    - DEDIPASS_PUBLIC_KEY : la clé publique ;
    - DEDIPASS_PRIVATE_KEY : la clé privée.

from datetime import datetime
import math

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Server(models.Model):
    """
    Un serveur. Proposé dans la boutique.
    """
    name = models.CharField(_('Nom'), max_length=48)
    ip = models.CharField(_('IP'), max_length=15)
    port = models.PositiveIntegerField(_('Port'))
    password = models.CharField(_('Mot de passe'), max_length=64, blank=True)
    description = models.TextField(_('Description'), blank=True)
    active = models.BooleanField(_('Actif'), default=True)
    proxy = models.BooleanField(_('Proxy'), default=False)
    position = models.IntegerField(_('Position'), default=0)
    slug = models.SlugField(_('Slug'), max_length=48, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Serveur')
        verbose_name_plural = _('Serveurs')
        ordering = ('position',)

    @staticmethod
    def get_proxy():
        try:
            return Server.objects.get(proxy=True)
        except:
            return None


class Category(models.Model):
    """
    Une catégorie. Appartient à un serveur. Permet de trier les produits.
    """
    name = models.CharField(_('Nom'), max_length=64)
    server = models.ForeignKey(Server, on_delete=models.CASCADE,
                               verbose_name=_('Serveur'))
    active = models.BooleanField(_('Actif'), default=True)
    position = models.IntegerField(_('Position'), default=0)
    slug = models.SlugField(_('Slug'), max_length=48, unique=True)

    def __str__(self):
        return _('{name} (serveur : {server_name})').format(
            name=self.name,
            server_name=self.server.name)

    class Meta:
        verbose_name = _('Catégorie')
        verbose_name_plural = _('Catégories')
        ordering = ('position',)


class Product(models.Model):
    """
    Un produit. Peut être acheté par un utilisateur contre des crédits.
    """
    name = models.CharField(_('Nom'), max_length=96)
    price = models.PositiveIntegerField(_('Prix'))
    short_description = models.TextField(_('Description courte'), blank=True)
    description = models.TextField(_('Description'), blank=True)
    image = models.ImageField(_('Image'), upload_to='./', blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE,
                                 verbose_name=_('Catégorie'))
    active = models.BooleanField(_('Actif'), default=True)
    position = models.IntegerField(_('Position'), default=0)
    need_logged = models.BooleanField(_('Besoin d\'être connecté'),
                                      default=False,
                                      help_text='Le joueur a-t-il besoin '
                                                'd\'être connecté au serveur '
                                                'au moment de l\'achat ?')
    slug = models.SlugField(_('Slug'), max_length=96, unique=True)

    def __str__(self):
        return _('{name} (serveur : {server_name}, '
                 'catégorie : {category_name})').format(
            name=self.name,
            server_name=self.category.server.name,
            category_name=self.category.name)

    def has_current_promotion(self):
        current_promotion = self.get_current_promotion()

        if current_promotion:
            return True
        else:
            return False

    def get_current_promotion(self):
        promotions = self.productpromotion_set.all().order_by('-id')

        active_promotions = []

        for promotion in promotions:
            # @see https://stackoverflow.com/a/32926295
            tzinfo = promotion.start_date.tzinfo
            datetime_now = datetime.now(tzinfo)

            if promotion.start_date <= datetime_now <= promotion.end_date:
                active_promotions.append(promotion)

        if len(active_promotions) >= 1:
            return active_promotions[0]
        else:
            return None

    def get_final_price(self):
        current_promotion = self.get_current_promotion()

        if current_promotion:
            return current_promotion.get_final_price()
        else:
            return self.price

    class Meta:
        verbose_name = _('Produit')
        verbose_name_plural = _('Produits')
        ordering = ('position',)


class Command(models.Model):
    """
    Une commande. Elle est liée à un produit qui peut en avoir plusieurs.
    """
    command = models.CharField(_('Commande'), max_length=512,
                               help_text='Sans la barre oblique initiale '
                                         '(slash) « / ». `&lt;player&gt;` est '
                                         'remplacé par le pseudonyme du '
                                         'joueur')
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                verbose_name=_('Produit'),)
    server = models.ForeignKey(Server, on_delete=models.CASCADE,
                               verbose_name=_('Serveur'), null=True,
                               blank=True,
                               help_text='Si aucun serveur n\'est '
                                         'sélectionné, ce sera le serveur de '
                                         'la catégorie qui sera utilisé')

    def __str__(self):
        return self.command

    class Meta:
        verbose_name = _('Commande')
        verbose_name_plural = _('Commandes')


class WitrushCommand(models.Model):
    """
    Une modification de valeur Witrush.
    """
    METHODS = (
        ('RPL', _('Remplacer')),
        ('ADD', _('Ajouter')),
    )
    FIELDS = (
        ('money', _('Money')),
        ('reduc_spawn', _('Reduc spawn')),
        ('pnj_speed', _('PNJ speed')),
        ('enderchest', _('Enderchest')),
        ('haste', _('Haste')),
        ('antiwither', _('Anti-wither')),
        ('good_start', _('Good start')),
        ('bettervision', _('Better vision')),
    )
    method = models.CharField(_('Méthode'), max_length=3, choices=METHODS,
                              default='RPL')
    field = models.CharField(_('Champ'), max_length=24, choices=FIELDS,
                             default='money')
    value = models.IntegerField(_('Valeur'), default=0)
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                verbose_name=_('Produit'))

    def __str__(self):
        if self.method == 'RPL':
            return _('Remplacer {field} par {value}').format(
                field=dict(self.FIELDS)[self.field],
                value=self.value)
        elif self.method == 'ADD':
            return _('Ajouter {field} à {value}').format(
                field=dict(self.FIELDS)[self.field],
                value=self.value)
        else:
            return _('{method}, {field}, {value}').format(
                method=dict(self.METHODS)[self.method],
                field=dict(self.FIELDS)[self.field],
                value=self.value)

    class Meta:
        verbose_name = _('Commande Witrush')
        verbose_name_plural = _('Commandes Witrush')


class ProductPromotion(models.Model):
    """
    Une promotion de produit.
    """
    METHODS = (
        ('PCT', _('Pourcentage')),
        ('FXD', _('Fixe')),
    )
    method = models.CharField(_('Méthode'), max_length=3, choices=METHODS,
                              default='PCT')
    value = models.IntegerField(_('Valeur'), default=0)
    start_date = models.DateTimeField(_('Date de début'), default=datetime.now)
    end_date = models.DateTimeField(_('Date de fin'), default=datetime.now)
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                verbose_name=_('Produit'))

    def __str__(self):
        if self.method == 'PCT':
            return _('Promotion de {value} pourcent').format(
                value=self.value)
        elif self.method == 'FXD':
            return _('Promotion de {value} crédits').format(
                value=self.value)
        else:
            return _('{method}, {value}').format(
                method=dict(self.METHODS)[self.method],
                value=self.value)

    def get_final_price(self):
        if self.method == 'PCT':
            discount = math.floor(self.product.price * (self.value / 100))
            return self.product.price - discount
        elif self.method == 'FXD':
            return self.product.price - self.value
        else:
            return self.product.price

    class Meta:
        verbose_name = _('Promotion')
        verbose_name_plural = _('Promotions')


class PaypalOffer(models.Model):
    """
    Une offre PayPal.
    """
    name = models.CharField(_('Nom'), max_length=128)
    credits_given = models.PositiveIntegerField(_('Crédits donnés'))
    price = models.DecimalField(_('Prix'), max_digits=6, decimal_places=2)
    active = models.BooleanField(_('Active'), default=True)
    position = models.IntegerField(_('Position'), default=0)
    slug = models.SlugField(_('Slug'), max_length=128, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Offre PayPal')
        verbose_name_plural = _('Offre PayPal')
        ordering = ('position',)


class ProductPayment(models.Model):
    """
    Un achat de produit.
    """
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                             verbose_name=_('Utilisateur'))
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True,
                                verbose_name=_('Produit'))
    category = models.ForeignKey(Category, on_delete=models.SET_NULL,
                                 null=True, verbose_name=_('Catégorie'))
    server = models.ForeignKey(Server, on_delete=models.SET_NULL, null=True,
                               verbose_name=_('Serveur'))
    email = models.EmailField(_('Adresse électronique'))
    minecraft_username = models.CharField(_('Pseudonyme Minecraft'),
                                          max_length=150)
    product_name = models.CharField(_('Nom du produit'), max_length=96)
    price_paid = models.PositiveIntegerField(_('Prix payé'))
    initial_price = models.PositiveIntegerField(_('Prix initial'))
    user_ip = models.CharField(_('IP de l\'utilisateur'), max_length=15)
    created_at = models.DateTimeField(_('Créé le'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Mis à jour le'), auto_now=True)

    def __str__(self):
        return _('{email}, {product} pour {price} crédits le {date}').format(
            email=self.email,
            product=self.product,
            price=self.price_paid,
            date=self.created_at)

    class Meta:
        verbose_name = _('Paiement de produit')
        verbose_name_plural = _('Paiements de produits')


class ProductPaymentCommand(models.Model):
    """
    Commande exécutée à l'achat d'un produit.
    """
    command = models.CharField(_('Commande'), max_length=512)
    product_payment = models.ForeignKey(ProductPayment,
                                        on_delete=models.CASCADE,
                                        verbose_name=_('Paiements de produit'))
    server = models.ForeignKey(Server, on_delete=models.CASCADE,
                               verbose_name=_('Serveur'), null=True,
                               blank=True)

    def __str__(self):
        return self.command

    class Meta:
        verbose_name = _('Commande de paiements de produit')
        verbose_name_plural = _('Commandes de paiements de produits')


class Witrush(models.Model):
    name = models.CharField(_('name'), max_length=16)
    uuid = models.CharField(_('uuid'), max_length=50)
    reduc_spawn = models.IntegerField(_('Reduc spawn'), default=0)
    pnj_speed = models.IntegerField(_('PNJ speed'), default=0)
    enderchest = models.IntegerField(_('Enderchest'), default=0)
    haste = models.IntegerField(_('Haste'), default=0)
    antiwither = models.IntegerField(_('Anti-wither'), default=0)
    good_start = models.IntegerField(_('Good start'), default=0)
    bettervision = models.IntegerField(_('Better vision'), default=0)
    money = models.IntegerField(_('Money'), default=0)

    def __str__(self):
        return self.uuid

    class Meta:
        app_label = 'witrush'
        db_table = 'witrush'
        verbose_name = _('Witrush')
        verbose_name_plural = _('Witrushs')


class ProductPaymentWitrushCommand(models.Model):
    """
    Commande Witrush exécutée à l'achat d'un produit.
    """
    method = models.CharField(_('Méthode'), max_length=3)
    field = models.CharField(_('Champ'), max_length=24)
    value = models.IntegerField(_('Valeur'), default=0)
    product_payment = models.ForeignKey(ProductPayment,
                                        on_delete=models.CASCADE,
                                        verbose_name=_('Paiements de produit'))

    def __str__(self):
        if self.method == 'RPL':
            return _('Remplacer {field} par {value}').format(
                field=dict(WitrushCommand.FIELDS)[self.field],
                value=self.value)
        elif self.method == 'ADD':
            return _('Ajouter {field} à {value}').format(
                field=dict(WitrushCommand.FIELDS)[self.field],
                value=self.value)
        else:
            return _('{method}, {field}, {value}').format(
                method=dict(WitrushCommand.METHODS)[self.method],
                field=dict(WitrushCommand.FIELDS)[self.field],
                value=self.value)

    class Meta:
        verbose_name = _('Commande Witrush de paiements de produit')
        verbose_name_plural = _('Commandes Witrush de paiements de produit')


class ProductPaymentPromotion(models.Model):
    """
    Une promotion de produit.
    """
    method = models.CharField(_('Méthode'), max_length=3)
    value = models.IntegerField(_('Valeur'), default=0)
    start_date = models.DateTimeField(_('Date de début'), default=datetime.now)
    end_date = models.DateTimeField(_('Date de fin'), default=datetime.now)
    product_payment = models.ForeignKey(ProductPayment,
                                        on_delete=models.CASCADE,
                                        verbose_name=_('Paiements de produit'))

    def __str__(self):
        if self.method == 'FXD':
            return _('Promotion de {value} crédits').format(
                value=self.value)
        elif self.method == 'PCT':
            return _('Promotion de {value} pourcent').format(
                value=self.value)
        else:
            return _('{method}, {value}').format(
                method=self.method,
                value=self.value)

    class Meta:
        verbose_name = _('Promotion de paiements de produit')
        verbose_name_plural = _('Promotions de paiements de produit')


class PaypalPayment(models.Model):
    """
    Un paiement PayPal.
    """
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                             verbose_name=_('Utilisateur'))
    email = models.EmailField(_('Courriel'))
    paypal_offer = models.ForeignKey(PaypalOffer, on_delete=models.SET_NULL,
                                     null=True, verbose_name=_('Offre PayPal'))
    credits_given = models.PositiveIntegerField(_('Crédits donnés'))
    price_paid = models.DecimalField(_('Prix payé'), max_digits=6,
                                     decimal_places=2)
    received_money = models.DecimalField(_('Argent reçu'), max_digits=6,
                                         decimal_places=2)
    user_ip = models.CharField(_('IP de l\'utilisateur'), max_length=15)
    ipn = models.ForeignKey('PaypalIpn', on_delete=models.CASCADE,
                            verbose_name=_('IPN PayPal'))
    finished = models.BooleanField(_('Terminé'), default=False)
    error_reason = models.TextField(_('Raison de l\'erreur'), blank=True)
    txn_id = models.CharField(max_length=32, blank=True)
    created_at = models.DateTimeField(_('Créé le'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Modifié le'), auto_now=True)

    def __str__(self):
        return _('Paiement PayPal par {email}, {credits} crédits pour '
                 '{price_paid} € le {date}').format(
            email=self.email,
            credits=self.credits_given,
            price_paid=self.price_paid,
            date=self.created_at)

    class Meta:
        verbose_name = _('Paiement PayPal')
        verbose_name_plural = _('Paiements PayPal')


class DedipassPayment(models.Model):
    """
    Un paiement Dedipass.
    """
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                             verbose_name=_('Utilisateur'))
    email = models.EmailField(_('Courriel'))
    received_money = models.DecimalField(_('Argent reçu'), max_digits=6,
                                         decimal_places=2)
    credits_given = models.PositiveIntegerField(_('Crédits donnés'))
    raw = models.TextField(_('Data payment method'))
    user_ip = models.CharField(_('IP de l\'utilisateur'), max_length=15)
    rate = models.CharField(_('Taux'), max_length=64)
    code = models.CharField(_('Code'), max_length=16, blank=True)
    finished = models.BooleanField(_('Terminé'), default=False)
    error_reason = models.TextField(_('Raison de l\'erreur'), blank=True)
    created_at = models.DateTimeField(_('Créé le'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Modifié le'), auto_now=True)

    def __str__(self):
        return _('Paiement Dedipass par {email} le {date}, {credits} pour\
                  {received_money}').format(
            email=self.email,
            date=self.created_at,
            credits=self.credits_given,
            received_money=self.received_money)

    class Meta:
        verbose_name = _('Paiement Dedipass')
        verbose_name_plural = _('Paiements Dedipass')


class PaysafecardPayment(models.Model):
    """
    Un paiement paysafecard.
    """
    STATUSES = (
        ('W', _('En attente')),
        ('A', _('Accepté')),
        ('C', _('Annulé'))
    )

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                             verbose_name=_('Utilisateur'))
    email = models.EmailField(_('Courriel'))
    code = models.CharField(_('Code'), max_length=32)
    price_paid = models.DecimalField(_('Prix payé'), max_digits=6,
                                     decimal_places=2)
    received_money = models.DecimalField(_('Argent reçue'), max_digits=6,
                                         decimal_places=2)
    credits_given = models.PositiveIntegerField(_('Crédits données'))
    user_ip = models.CharField(_('IP de l\'utilisateur'), max_length=15)
    status = models.CharField(_('Statut'), max_length=1, choices=STATUSES,
                              default='W')
    cancellation_reason = models.TextField(_('Raison de l\'annulation'),
                                           blank=True)
    created_at = models.DateTimeField(_('Créé le'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Modifié le'), auto_now=True)

    def __str__(self):
        return _('Paiement paysafecard par {email} le {date}').format(
            email=self.email,
            date=self.created_at)

    class Meta:
        verbose_name = _('Paiement paysafecard')
        verbose_name_plural = _('Paiements paysafecard')


class PaypalIpn(models.Model):
    """
    Une notification PayPal.
    """
    protection_eligibility = models.CharField(max_length=64, blank=True)
    last_name = models.CharField(max_length=64, blank=True)
    txn_id = models.CharField(max_length=32, blank=True)
    receiver_email = models.CharField(max_length=128, blank=True)
    payment_status = models.CharField(max_length=64, blank=True)
    payment_gross = models.CharField(max_length=32, blank=True)
    tax = models.CharField(max_length=32, blank=True)
    residence_country = models.CharField(max_length=6, blank=True)
    invoice = models.CharField(max_length=64, blank=True)
    payer_status = models.CharField(max_length=32, blank=True)
    txn_type = models.CharField(max_length=64, blank=True)
    handling_amount = models.CharField(max_length=32, blank=True)
    payment_date = models.CharField(max_length=64, blank=True)
    first_name = models.CharField(max_length=64, blank=True)
    item_name = models.CharField(max_length=128, blank=True)
    charset = models.CharField(max_length=32, blank=True)
    custom = models.CharField(max_length=256, blank=True)
    notify_version = models.CharField(max_length=32, blank=True)
    transaction_subject = models.CharField(max_length=128, blank=True)
    test_ipn = models.CharField(max_length=2, blank=True)
    item_number = models.CharField(max_length=128, blank=True)
    receiver_id = models.CharField(max_length=16, blank=True)
    business = models.CharField(max_length=128, blank=True)
    payer_id = models.CharField(max_length=16, blank=True)
    verify_sign = models.CharField(max_length=64, blank=True)
    payment_fee = models.CharField(max_length=16, blank=True)
    mc_fee = models.CharField(max_length=16, blank=True)
    mc_currency = models.CharField(max_length=16, blank=True)
    shipping = models.CharField(max_length=16, blank=True)
    payer_email = models.CharField(max_length=128, blank=True)
    payment_type = models.CharField(max_length=32, blank=True)
    mc_gross = models.CharField(max_length=16, blank=True)
    ipn_track_id = models.CharField(max_length=16, blank=True)
    quantity = models.CharField(max_length=16, blank=True)
    raw = models.TextField(blank=True)
    request_response = models.CharField(_('Réponse de la requête'),
                                        max_length=32, blank=True)
    created_at = models.DateTimeField(_('Créé le'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Modifié le'), auto_now=True)

    def __str__(self):
        return _('IPN PayPal : {txn_id}').format(txn_id=self.txn_id)

    class Meta:
        verbose_name = _('IPN PayPal')
        verbose_name_plural = _('IPN PayPal')


class MoneyOut(models.Model):
    """
    Sortie d'argent.
    """
    PLATFORMS = (
        ('PPL', _('PayPal')),
        ('BNK', _('Compte bancaire')),
        ('LQD', _('Liquide')),
        ('CHK', _('Chèque')),
        ('OTH', _('Autre')),
    )
    REASONS = (
        ('SVR', _('Paiement du serveur')),
        ('HOS', _('Paiement de l\'hébergement')),
        ('DNS', _('Paiement du nom de domaine')),
        ('YTA', _('Publicité sur YouTube')),
        ('ADV', _('Publicité')),
        ('RFD', _('Remboursement')),
        ('OPP', _('Opposition')),
        ('GFT', _('Cadeau')),
        ('PLS', _('Plaisir')),
        ('DEV', _('Paiement d\'un développement')),
        ('OTH', _('Autre')),
    )
    platform = models.CharField(_('Platforme'), max_length=3,
                                choices=PLATFORMS, default='OTH')
    amount = models.DecimalField(_('Montant'), max_digits=6,
                                 decimal_places=2)
    release_date = models.DateTimeField(_('Date'), auto_now_add=True)
    reason = models.CharField(_('Raison'), max_length=3, choices=REASONS,
                              default='OTH')
    label = models.CharField(_('Label'), max_length=128, blank=True)
    note = models.TextField(_('Note'), blank=True)

    def __str__(self):
        return _('Sortie d\'argent : {label}').format(label=self.label)

    class Meta:
        verbose_name = _('Sortie d\'argent')
        verbose_name_plural = _('Sorties d\'argent')


class MoneyIncome(models.Model):
    """
    Rentrée d'argent.
    """
    PLATFORMS = (
        ('PPL', _('PayPal')),
        ('BNK', _('Compte bancaire')),
        ('LQD', _('Liquide')),
        ('CHK', _('Chèque')),
        ('ADS', _('AdSense')),
        ('OTH', _('Autre')),
    )
    REASONS = (
        ('NON', _('Aucune')),
        ('DON', _('Don')),
        ('OTH', _('Autre')),
    )
    platform = models.CharField(_('Platforme'), max_length=3,
                                choices=PLATFORMS, default='OTH')
    amount = models.DecimalField(_('Montant'), max_digits=6,
                                 decimal_places=2)
    release_date = models.DateTimeField(_('Date'), auto_now_add=True)
    reason = models.CharField(_('Raison'), max_length=3, choices=REASONS,
                              default='NON')
    label = models.CharField(_('Label'), max_length=128, blank=True)
    note = models.TextField(_('Note'), blank=True)

    def __str__(self):
        return _('Entrée d\'argent : {label}').format(label=self.label)

    class Meta:
        verbose_name = _('Entrée d\'argent')
        verbose_name_plural = _('Entrées d\'argent')

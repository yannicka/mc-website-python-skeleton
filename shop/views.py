import copy
import decimal
import json
import requests

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls.base import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from . import utils
from .models import Server, Category, Product, PaypalOffer, Command,\
    WitrushCommand, ProductPayment, ProductPaymentCommand,\
    Witrush, ProductPaymentWitrushCommand, ProductPaymentPromotion,\
    PaysafecardPayment, PaypalPayment
from shop import models
from shop.models import DedipassPayment
from website.settings import DEDIPASS_PRIVATE_KEY, DEDIPASS_PUBLIC_KEY,\
    PAYPAL_RECEIVER_EMAIL, PAYPAL_TEST, PAYPAL_URL, PAYPAL_URL_SANDBOX,\
    BASE_URL


def index(request):
    servers = Server.objects.filter(active=True)

    context = {
        'title_tab': _('Boutique'),
        'title_page': _('Boutique'),
        'active_tab': 'shop',
        'servers': servers,
    }

    return render(request, 'shop/index.html', context)


def list_categories(request, server_slug):
    server = get_object_or_404(Server, slug=server_slug)
    categories = Category.objects.filter(server=server, active=True)
    products = Product.objects.filter(category__in=categories, active=True)

    if not server.active:
        return redirect('shop.index')

    context = {
        'title_tab': _('Produits du serveur {name} - Boutique').format(
            name=server.name),
        'title_page': _('Produits du serveur {name}').format(name=server.name),
        'active_tab': 'shop',
        'categories': categories,
        'products': products,
    }

    return render(request, 'shop/list-categories.html', context)


@login_required
def buy_item(request, slug):
    product = get_object_or_404(Product, slug=slug)
    price = product.get_final_price()
    user_credits = request.user.member.credits
    server = product.category.server

    if not product.active or price > user_credits:
        return redirect('shop.index')

    credits_after_buying = request.user.member.credits - price

    context = {
        'title_tab': _('Acheter un produit'),
        'title_page': _('Acheter un produit'),
        'active_tab': 'shop',
        'product': product,
        'server': server,
        'credits_after_buying': credits_after_buying,
    }

    return render(request, 'shop/buy-item.html', context)


@login_required
@csrf_exempt
def validate_username(request, server_slug, minecraft_username):
    return HttpResponse('1')


@login_required
@require_POST
def valid_buy_item(request):
    product = Product.objects.get(slug=request.POST['slug'])
    price = product.get_final_price()
    user = request.user

    if not product.active or price > user.member.credits:
        return redirect('shop.index')

    minecraft_username = request.POST['minecraft-username']
    minecraft_uuid = utils.get_uuid_from_minecraft_username(minecraft_username)
    commands = product.command_set.all()
    witrush_commands = product.witrushcommand_set.all()
    promotions = product.productpromotion_set.all()

    payment = ProductPayment()
    payment.user = request.user
    payment.product = product
    payment.category = product.category
    payment.server = product.category.server
    payment.email = request.user.email
    payment.minecraft_username = minecraft_username
    payment.product_name = product.name
    payment.price_paid = price
    payment.initial_price = product.price
    payment.user_ip = utils.get_client_ip(request)
    payment.save()

    final_commands = []

    for command in commands:
        final_command = copy.deepcopy(command)
        final_command.command = final_command.command.replace(
            '<player>',
            minecraft_username
        )

        c = ProductPaymentCommand()
        c.command = final_command.command
        c.product_payment = payment
        c.server = final_command.server
        c.save()

        final_commands.append(final_command)

    for command in witrush_commands:
        try:
            witrush = Witrush.objects.get(uuid=minecraft_uuid)

            if command.method == 'RPL':
                setattr(witrush, command.field, command.value)
            else:  # ADD ou autre
                setattr(witrush,
                        command.field,
                        getattr(witrush, command.field) + command.value)

            witrush.save()

            wc = ProductPaymentWitrushCommand()
            wc.method = command.method
            wc.field = command.field
            wc.value = command.value
            wc.product_payment = payment
            wc.save()
        except:
            pass

    for promotion in promotions:
        p = ProductPaymentPromotion()
        p.method = promotion.method
        p.value = promotion.value
        p.start_date = promotion.start_date
        p.end_date = promotion.end_date
        p.product_payment = payment
        p.save()

    user.member.credits -= price
    user.member.save()

    for command in final_commands:
        utils.exec_minecraft_command(product, command)

    return redirect('shop.confirm-buy-item')


@csrf_exempt
def confirm_buy_item(request):
    context = {
        'title_tab': _('Confirmation de paiement de produit'),
        'title_page': _('Confirmation de paiement de produit'),
        'active_tab': 'shop',
    }

    return render(request, 'shop/confirm-buy-item.html', context)


@login_required
def buy_credits(request):
    context = {
        'title_tab': _('Acheter des crédits'),
        'title_page': _('Acheter des crédits'),
        'active_tab': 'shop',
    }

    return render(request, 'shop/buy-credits.html', context)


@login_required
def buy_credits_paypal(request):
    id_invoice = utils.random_string(length=32)

    if PAYPAL_TEST:
        paypal_url = PAYPAL_URL_SANDBOX
    else:
        paypal_url = PAYPAL_URL

    paypal_offers = PaypalOffer.objects.filter(active=True)

    context = {
        'title_tab': _('Acheter des crédits avec PayPal'),
        'title_page': _('Acheter des crédits avec PayPal'),
        'active_tab': 'shop',
        'base_url': BASE_URL,
        'paypal_url': paypal_url,
        'paypal_offers': paypal_offers,
        'paypal_receiver_email': PAYPAL_RECEIVER_EMAIL,
        'id_invoice': id_invoice,
        'tos_url': reverse('terms-of-sales'),
    }

    return render(request, 'shop/buy-credits-paypal.html', context)


@login_required
def buy_credits_dedipass(request):
    context = {
        'title_tab': _('Acheter des crédits avec Dedipass'),
        'title_page': _('Acheter des crédits avec Dedipass'),
        'active_tab': 'shop',
        'dedipass_public_key': DEDIPASS_PUBLIC_KEY,
        'tos_url': reverse('terms-of-sales'),
    }

    return render(request, 'shop/buy-credits-dedipass.html', context)


@login_required
def buy_credits_paysafecard(request):
    context = {
        'title_tab': _('Acheter des crédits avec paysafecard'),
        'title_page': _('Acheter des crédits avec paysafecard'),
        'active_tab': 'shop',
        'tos_url': reverse('terms-of-sales'),
    }

    return render(request, 'shop/buy-credits-paysafecard.html', context)


@csrf_exempt
@require_POST
def validate_credits_paypal(request):
    # Validation du paiement
    query = request.body.decode()

    data = 'cmd=_notify-validate&{query}'.format(query=query)

    if 'test_ipn=1' in data:
        url = PAYPAL_URL_SANDBOX
    else:
        url = PAYPAL_URL

    req = requests.post(url, data=data)
    res = req.content.decode()

    ipn = request.POST

    paypal_ipn = models.PaypalIpn()
    paypal_ipn.protection_eligibility = ipn.get('protection_eligibility', '')
    paypal_ipn.last_name = ipn.get('last_name', '')
    paypal_ipn.txn_id = ipn.get('txn_id', '')
    paypal_ipn.receiver_email = ipn.get('receiver_email', '')
    paypal_ipn.payment_status = ipn.get('payment_status', '')
    paypal_ipn.payment_gross = ipn.get('payment_gross', '')
    paypal_ipn.tax = ipn.get('tax', '')
    paypal_ipn.residence_country = ipn.get('residence_country', '')
    paypal_ipn.invoice = ipn.get('invoice', '')
    paypal_ipn.payer_status = ipn.get('payer_status', '')
    paypal_ipn.txn_type = ipn.get('txn_type', '')
    paypal_ipn.handling_amount = ipn.get('handling_amount', '')
    paypal_ipn.payment_date = ipn.get('payment_date', '')
    paypal_ipn.first_name = ipn.get('first_name', '')
    paypal_ipn.item_name = ipn.get('item_name', '')
    paypal_ipn.charset = ipn.get('charset', '')
    paypal_ipn.custom = ipn.get('custom', '')
    paypal_ipn.notify_version = ipn.get('notify_version', '')
    paypal_ipn.transaction_subject = ipn.get('transaction_subject', '')
    paypal_ipn.test_ipn = ipn.get('test_ipn', '')
    paypal_ipn.item_number = ipn.get('item_number', '')
    paypal_ipn.receiver_id = ipn.get('receiver_id', '')
    paypal_ipn.business = ipn.get('business', '')
    paypal_ipn.payer_id = ipn.get('payer_id', '')
    paypal_ipn.verify_sign = ipn.get('verify_sign', '')
    paypal_ipn.payment_fee = ipn.get('payment_fee', '')
    paypal_ipn.mc_fee = ipn.get('mc_fee', '')
    paypal_ipn.mc_currency = ipn.get('mc_currency', '')
    paypal_ipn.shipping = ipn.get('shipping', '')
    paypal_ipn.payer_email = ipn.get('payer_email', '')
    paypal_ipn.payment_type = ipn.get('payment_type', '')
    paypal_ipn.mc_gross = ipn.get('mc_gross', '')
    paypal_ipn.ipn_track_id = ipn.get('ipn_track_id', '')
    paypal_ipn.quantity = ipn.get('quantity', '')
    paypal_ipn.raw = json.dumps(ipn, ensure_ascii=False)
    paypal_ipn.request_response = res
    paypal_ipn.save()

    # Vérification de la réponse de la requête
    if res != 'VERIFIED':
        return

    # Vérification du statut de la commande
    if ipn.get('payment_status', '') != 'Completed':
        return

    # Création du paiement PayPal
    payment = PaypalPayment()
    payment.user = None
    payment.email = ''
    payment.paypal_offer = None
    payment.credits_given = 0
    payment.price_paid = 0
    payment.received_money = 0
    payment.user_ip = utils.get_client_ip(request)
    payment.ipn = paypal_ipn
    payment.finished = False
    payment.error_reason = ''
    payment.save()

    custom = ipn.get('custom', '')
    id_user, id_paypal_offer = custom.split('|')

    # Si l'utilisateur n'existe pas, annulation
    try:
        id_user = int(id_user)
    except ValueError:
        id_user = 0

    try:
        user = User.objects.get(pk=id_user)
    except User.DoesNotExist:
        payment.error_reason = 'L\'utilisateur « {id_user} » n\'existe pas'\
            .format(id_user=id_user)
        payment.save()

        return

    payment.user = user
    payment.email = user.email
    payment.save()

    # Si l'offre PayPal n'existe pas, annulation
    try:
        id_paypal_offer = int(id_paypal_offer)
    except ValueError:
        id_paypal_offer = 0

    try:
        paypal_offer = PaypalOffer.objects.get(pk=id_paypal_offer)
    except PaypalOffer.DoesNotExist:
        payment.error_reason = 'L\'offre PayPal « {id_offer} » n\'existe pas'\
            .format(id_offer=id_paypal_offer)
        payment.save()

        return

    if not paypal_offer.active:
        payment.error_reason = 'L\'offre PayPal « {id_offer} » n\'est pas' \
                               'active'\
            .format(id_offer=id_paypal_offer)
        payment.save()

        return

    credits_given = paypal_offer.credits_given

    payment.paypal_offer = paypal_offer
    payment.credits_given = credits_given
    payment.save()

    # Le prix payé doit être égal à celui de l'offre
    try:
        price_paid = decimal.Decimal(ipn.get('mc_gross', '0'))
    except ValueError:
        price_paid = decimal.Decimal('0.0')

    payment.price_paid = price_paid

    # Argent reçu
    try:
        received_money = price_paid
        received_money -= decimal.Decimal(ipn.get('mc_fee', '0'))
    except ValueError:
        received_money = decimal.Decimal('0.0')

    payment.received_money = received_money
    payment.save()

    if price_paid != paypal_offer.price:
        payment.error_reason = 'Le prix payé ({price_paid}) ne correspond '\
                               'pas au prix de l\'offre ({price_offer})'\
            .format(price_paid=price_paid, price_offer=paypal_offer.price)
        payment.save()

        return

    # Si le paiement est un doublon, annulation
    txn_id = ipn.get('txn_id', '')

    try:
        txn_payment = PaypalPayment.objects.get(txn_id=txn_id)
    except PaypalPayment.DoesNotExist:
        txn_payment = None

    if txn_payment is not None:
        payment.error_reason = 'Duplication de l\'ID txn {txn_id}'\
            .format(txn_id=txn_id)
        payment.save()

        return

    # Vérification du compte PayPal crédité
    if ipn.get('receiver_email', '') != PAYPAL_RECEIVER_EMAIL:
        payment.error_reason = 'L\'e-mail du receveur de l\'argent n\'est '\
                               'pas le bon ({bad_email} au lieu de '\
                               '{good_email}'\
            .format(bad_email=ipn.get('receiver_email', ''),
                    good_email=PAYPAL_RECEIVER_EMAIL)
        payment.save()

        return

    payment.txn_id = ipn.get('txn_id', '')
    payment.finished = True
    payment.save()

    utils.give_user_credits(user, credits_given)

    return HttpResponse('OKAY')


@csrf_exempt
@require_POST
def validate_credits_dedipass(request):
    # Paramètre         Description
    # ---------         -----------
    # status            success/invalid
    # code              code saisi par l'utilisateur (celui envoyé par SMS)
    # rate              palier utilisé
    # payout            reversement en €
    # virtual_currency  nombre de crédits

    code = request.POST.get('code', '')

    if code == '':
        return redirect('home')

    url = 'http://api.dedipass.com/v1/pay/'\
          '?public_key={public_key}&private_key={private_key}&code={code}'\
        .format(
            public_key=DEDIPASS_PUBLIC_KEY,
            private_key=DEDIPASS_PRIVATE_KEY,
            code=code
        )

    dedipass = requests.get(url).text
    dump = json.loads(dedipass)

    if dump.get('status', 'empty') == 'success':
        rate = dump.get('rate', 'empty')

        try:
            credits_given = int(dump.get('virtual_currency', '0'))
        except ValueError:
            credits_given = 0

        payment = DedipassPayment()
        payment.user = None
        payment.email = ''
        payment.received_money = decimal.Decimal(dump.get('payout', 0))
        payment.credits_given = credits_given
        payment.raw = dedipass
        payment.user_ip = utils.get_client_ip(request)
        payment.rate = rate
        payment.code = dump.get('code', '')
        payment.finished = False
        payment.error_reason = ''

        # Si l'utilisateur n'existe pas, annulation
        id_user = request.POST.get('custom', '0')

        try:
            id_user = int(id_user)
        except ValueError:
            id_user = 0

        try:
            user = User.objects.get(pk=id_user)
        except User.DoesNotExist:
            payment.error_reason = 'L\'utilisateur n\'existe pas'
            payment.save()

            return

        payment.user = user
        payment.email = user.email
        payment.finished = True
        payment.save()

        utils.give_user_credits(user, credits_given)

        return redirect('shop.confirm-credits-dedipass')
    else:
        return redirect('home')


@csrf_exempt
@login_required
@require_POST
def validate_credits_paysafecard(request):
    part1 = request.POST.get('code-part1')
    part2 = request.POST.get('code-part2')
    part3 = request.POST.get('code-part3')
    part4 = request.POST.get('code-part4')
    read_tos = request.POST.get('read-tos', False)

    if not read_tos:
        return redirect('shop.buy-credits-paysafecard')

    payment = PaysafecardPayment()
    payment.user = request.user
    payment.email = request.user.email
    payment.code = part1 + '-' + part2 + '-' + part3 + '-' + part4
    payment.price_paid = 0
    payment.received_money = 0
    payment.credits_given = 0
    payment.user_ip = utils.get_client_ip(request)
    payment.save()

    return redirect('shop.confirm-credits-paysafecard')


@csrf_exempt
def confirm_credits_paypal(request):
    context = {
        'title_tab': _('Confirmation de paiement avec PayPal'),
        'title_page': _('Confirmation de paiement avec PayPal'),
        'active_tab': 'shop',
    }

    return render(request, 'shop/confirm-credits-paypal.html', context)


def confirm_credits_dedipass(request):
    context = {
        'title_tab': _('Confirmation de paiement avec Dedipass'),
        'title_page': _('Confirmation de paiement avec Dedipass'),
        'active_tab': 'shop',
    }

    return render(request, 'shop/confirm-credits-dedipass.html', context)


def confirm_credits_paysafecard(request):
    context = {
        'title_tab': _('Confirmation de paiement avec paysafecard'),
        'title_page': _('Confirmation de paiement avec paysafecard'),
        'active_tab': 'shop',
    }

    return render(request, 'shop/confirm-credits-paysafecard.html', context)


@csrf_exempt
def cancel_credits_paypal(request):
    context = {
        'title_tab': _('Annulation du paiement PayPal'),
        'title_page': _('Annulation du paiement PayPal'),
        'active_tab': 'shop',
    }

    return render(request, 'shop/cancel-credits-paypal.html', context)

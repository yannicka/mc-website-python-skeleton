from django.contrib import admin

from .models import Server, Category, Product, Command, WitrushCommand,\
    ProductPromotion, ProductPayment, ProductPaymentCommand,\
    ProductPaymentWitrushCommand, ProductPaymentPromotion, PaysafecardPayment,\
    PaypalOffer, PaypalPayment, DedipassPayment, PaypalIpn, MoneyOut,\
    MoneyIncome
from .utils import give_user_credits


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    list_display = ('name', 'ip', 'port', 'active', 'proxy', 'position')
    list_editable = ('active', 'proxy', 'position')
    list_filter = ('active',)
    search_fields = ('name',)
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'server', 'active', 'position')
    list_editable = ('active', 'position')
    list_filter = ('server', 'active')
    search_fields = ('name', 'slug', 'server__name')
    prepopulated_fields = {'slug': ('name',)}


class CommandInline(admin.StackedInline):
    model = Command
    extra = 1


class WitrushCommandInline(admin.TabularInline):
    model = WitrushCommand
    extra = 0


class ProductPromotionInline(admin.TabularInline):
    model = ProductPromotion
    extra = 0


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'category', 'active', 'position',
                    'need_logged')
    list_editable = ('active', 'position')
    list_filter = ('category', 'category__server', 'active', 'need_logged')
    list_per_page = 50
    search_fields = ('name', 'category__name')
    prepopulated_fields = {'slug': ('name',)}
    change_form_template = 'admin/website/product-form.html'

    inlines = [
        CommandInline,
        WitrushCommandInline,
        ProductPromotionInline,
    ]


@admin.register(PaypalOffer)
class PaypalOfferAdmin(admin.ModelAdmin):
    list_display = ('name', 'credits_given', 'price', 'active', 'position')
    list_editable = ('active', 'position')
    list_filter = ('active',)
    search_fields = ('name',)
    prepopulated_fields = {'slug': ('name',)}


class ProductPaymentCommandInline(admin.StackedInline):
    model = ProductPaymentCommand
    extra = 0


class ProductPaymentWitrushCommandInline(admin.TabularInline):
    model = ProductPaymentWitrushCommand
    extra = 0


class ProductPaymentPromotionInline(admin.TabularInline):
    model = ProductPaymentPromotion
    extra = 0


@admin.register(ProductPayment)
class ProductPaymentAdmin(admin.ModelAdmin):
    list_display = ('user', 'product_name', 'price_paid', 'minecraft_username',
                    'created_at')
    list_per_page = 50
    readonly_fields = ('created_at', 'updated_at')
    list_filter = ('created_at',)
    search_fields = ('user__email', 'product_name', 'minecraft_username')
    inlines = [
        ProductPaymentCommandInline,
        ProductPaymentWitrushCommandInline,
        ProductPaymentPromotionInline,
    ]


@admin.register(PaypalPayment)
class PaypalPaymentAdmin(admin.ModelAdmin):
    list_display = ('user', 'ipn_payer_email', 'paypal_offer', 'credits_given',
                    'received_money', 'created_at', 'finished')
    list_filter = ('finished', 'paypal_offer', 'received_money', 'created_at',
                   'updated_at')
    list_per_page = 50
    readonly_fields = ('created_at', 'updated_at')
    search_fields = ('user__email', 'ipn__payer_email', 'paypal_offer__name')

    def ipn_payer_email(self, obj):
        return obj.ipn.payer_email
    ipn_payer_email.short_description = 'Courriel acheteur PayPal'
    ipn_payer_email.admin_order_field = 'ipn_payer_email'


@admin.register(DedipassPayment)
class DedipassPaymentAdmin(admin.ModelAdmin):
    list_display = ('user', 'credits_given', 'received_money', 'created_at',
                    'finished')
    list_filter = ('credits_given', 'received_money', 'finished', 'created_at',
                   'error_reason')
    search_fields = ('user__email',)
    list_per_page = 50
    readonly_fields = ('created_at', 'updated_at')


@admin.register(PaysafecardPayment)
class PaysafecardPaymentAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'received_money', 'created_at',
                    'updated_at', 'status')
    list_filter = ('status', 'created_at', 'updated_at')
    list_per_page = 50
    readonly_fields = ('created_at', 'updated_at')
    search_fields = ('user__email', 'email', 'cancellation_reason')

    def save_model(self, request, obj, form, change):
        if 'status' in form.changed_data and request.POST['status'] == 'A':
            give_user_credits(obj.user, obj.credits_given)

        obj.save()


@admin.register(PaypalIpn)
class PaypalIpnAdmin(admin.ModelAdmin):
    list_display = ('txn_id', 'payer_email', 'payment_status',
                    'request_response', 'created_at')
    list_filter = ('payment_status', 'request_response', 'payment_status')
    readonly_fields = ('created_at', 'updated_at')
    search_fields = ('txn_id', 'payer_email', 'raw')
    list_per_page = 50


@admin.register(MoneyOut)
class MoneyOutAdmin(admin.ModelAdmin):
    list_display = ('label', 'release_date', 'platform', 'reason', 'amount')
    list_filter = ('platform', 'reason', 'release_date')
    search_fields = ('label', 'note')
    list_per_page = 50


@admin.register(MoneyIncome)
class MoneyIncomeAdmin(admin.ModelAdmin):
    list_display = ('label', 'release_date', 'platform', 'reason', 'amount')
    list_filter = ('platform', 'reason', 'release_date')
    search_fields = ('label', 'note')
    list_per_page = 50

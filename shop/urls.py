from django.conf.urls import url, include

from . import views


urlpatterns = [
    url(r'^$', views.index,
        name='shop.index'),

    url(r'^list-categories/(?P<server_slug>[\w-]+)/$', views.list_categories,
        name='shop.list-categories'),

    url(r'^buy-item/(?P<slug>[\w-]+)/$', views.buy_item,
        name='shop.buy-item'),
    url(r'^validate-username/(?P<server_slug>[\w-]+)/(?P<minecraft_username>[\w-]+)/$',
        views.validate_username, name='shop.validate-username'),
    url(r'^valid-buy-item/$', views.valid_buy_item,
        name='shop.valid-buy-item'),
    url(r'^confirm-buy-item/$', views.confirm_buy_item,
        name='shop.confirm-buy-item'),

    url(r'^buy-credits/$', views.buy_credits,
        name='shop.buy-credits'),
    url(r'^buy-credits-paypal/$', views.buy_credits_paypal,
        name='shop.buy-credits-paypal'),
    url(r'^buy-credits-dedipass/$', views.buy_credits_dedipass,
        name='shop.buy-credits-dedipass'),
    url(r'^buy-credits-paysafecard/$', views.buy_credits_paysafecard,
        name='shop.buy-credits-paysafecard'),

    url(r'^validate-credits-paypal/$', views.validate_credits_paypal,
        name='shop.validate-credits-paypal'),
    url(r'^validate-credits-dedipass/$', views.validate_credits_dedipass,
        name='shop.validate-credits-dedipass'),
    url(r'^validate-credits-paysafecard/$', views.validate_credits_paysafecard,
        name='shop.validate-credits-paysafecard'),

    url(r'^confirm-credits-paypal/$', views.confirm_credits_paypal,
        name='shop.confirm-credits-paypal'),
    url(r'^confirm-credits-dedipass/$', views.confirm_credits_dedipass,
        name='shop.confirm-credits-dedipass'),
    url(r'^confirm-credits-paysafecard/$', views.confirm_credits_paysafecard,
        name='shop.confirm-credits-paysafecard'),

    url(r'^cancel-credits-paypal/$', views.cancel_credits_paypal,
        name='shop.cancel-credits-paypal'),
]

import hashlib
import json
import random
import re
import requests
import string

from minecraft.rcon import Rcon


def give_user_credits(user, credits):
    user.member.credits += credits

    user.member.save()


def exec_minecraft_command(product, command):
    server = product.category.server

    if command.server:
        server = command.server

    rcon = Rcon()
    rcon.connect(server.ip, server.port, server.password)

    rcon.command(command.command)


def random_string(length, symbols=string.ascii_letters + string.digits):
    rand_str = ''.join(random.choice(symbols) for _ in range(length))

    return rand_str


def get_client_ip(request):
    try:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
    except:
        ip = '0.0.0.0'

    return ip


def get_uuid_from_minecraft_username(username):
    try:
        url = 'https://api.mojang.com/users/profiles/minecraft/'.format(
            username)
        result = requests.get(url).json()

        if 'id' in result:
            uuid = result['id']

            # Ajoute des tirets à la chaine de base
            uuid = re.sub(r'([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})',
                          r'\1-\2-\3-\4-\5',
                          uuid)

            return uuid
        else:
            return '-'
    except:
        return '-'
